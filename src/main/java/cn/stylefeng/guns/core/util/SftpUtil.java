package cn.stylefeng.guns.core.util;

import com.jcraft.jsch.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * 文件工具类.<br>
 * 1.所有的文件路径必须以'/'开头和结尾，否则路径最后一部分会被当做是文件名<br>
 * 2. @since version-0.3 方法出现异常的时候，<del>会关闭sftp连接(但是不会关闭session和channel)</del>(del @ version 0.31)，异常会抛出<br>
 *
 * @author Leon Lee
 */
public class SftpUtil {
    private final static Logger logger = LoggerFactory.getLogger(SftpUtil.class);

    /**
     * 文件路径前缀
     */
    private static final String PRE_FIX = "/data";

    /**
     * sftp连接池.
     */
    private static final Map<String, Channel> SFTP_CHANNEL_POOL = new HashMap<String, Channel>();

    /**
     * 获取sftp协议连接.
     *
     * @param host     主机名
     * @param port     端口
     * @param username 用户名
     * @param password 密码
     * @return 连接对象
     * @throws JSchException 异常
     */
    public static ChannelSftp getSftpConnect(final String host, final int port, final String username,
                                             final String password) throws JSchException {
        Session sshSession = null;
        Channel channel = null;
        ChannelSftp sftp = null;
//        if (null == SFTP_CHANNEL_POOL.get(key)) {
        JSch jsch = new JSch();
        jsch.getSession(username, host, port);
        sshSession = jsch.getSession(username, host, port);
        sshSession.setPassword(password);
        Properties sshConfig = new Properties();
        sshConfig.put("StrictHostKeyChecking", "no");
        sshSession.setConfig(sshConfig);
        sshSession.connect();
        channel = sshSession.openChannel("sftp");
        channel.connect();
//            SFTP_CHANNEL_POOL.put(key, channel);
//        } else {
//            channel = SFTP_CHANNEL_POOL.get(key);
//            sshSession = channel.getSession();
//            if (!sshSession.isConnected()) {
//                sshSession.connect();
//            }
//            if (!channel.isConnected()) {
//                channel.connect();
//            }
//        }
        sftp = (ChannelSftp) channel;
        return sftp;
    }

    /**
     * 上传文件-sftp协议.
     *
     * @param srcFile  源文件
     * @param dir      保存路径
     * @param fileName 保存文件名
     * @param sftp     sftp连接
     * @throws Exception 异常
     */
    public static void uploadFile(final String srcFile, final String dir, final String fileName, final ChannelSftp sftp)
            throws SftpException {
        mkdir(dir, sftp);
        sftp.cd(dir);
        sftp.put(srcFile, fileName);
    }

    /**
     * 上传文件-sftp协议.
     *
     * @param srcFile 源文件路径，/xxx/xx.yy 或 x:/xxx/xxx.yy
     * @param sftp    sftp连接
     * @return 上传成功与否
     * @throws SftpException 异常
     */
    public static boolean uploadFile(final String srcFile, final ChannelSftp sftp) throws SftpException {
        File file = new File(srcFile);
        if (file.exists()) {
            List<String> list = formatPath(srcFile);
            uploadFile(srcFile, list.get(0), list.get(1), sftp);
            return true;
        }
        return false;
    }

    /**
     * 上传文件-sftp协议.
     *
     * @param sftp sftp连接
     * @return 上传成功与否
     * @throws SftpException 异常
     */
    public static void uploadByte(final String srcJson, String filename, final ChannelSftp sftp) throws Exception {
        byte[] bytes = srcJson.getBytes();
        int b = bytes.length;

        OutputStream out = sftp.put("/data/" + filename + ".json", ChannelSftp.OVERWRITE);
        byte[] buff = new byte[1024 * 256];
        int read;
        if (out != null) {
            InputStream is = new ByteArrayInputStream(bytes);
            do {
                read = is.read(buff, 0, buff.length);
                if (read > 0) {
                    out.write(buff, 0, read);
                }
                out.flush();
            } while (read >= 0);
            logger.info("ftp stream write done.");
        }
    }

    /**
     * 根据路径创建文件夹.
     *
     * @param dir  路径 必须是 /xxx/xxx/ 不能就单独一个/
     * @param sftp sftp连接
     * @throws SftpException 异常
     */
    public static boolean mkdir(final String dir, final ChannelSftp sftp) throws SftpException {
        if (StringUtils.isBlank(dir)) {
            return false;
        }
        String md = dir.replaceAll("\\\\", "/");
        if (md.indexOf("/") != 0 || md.length() == 1) {
            return false;
        }
        return mkdirs(md, sftp);
    }

    /**
     * 递归创建文件夹.
     *
     * @param dir  路径
     * @param sftp sftp连接
     * @return 是否创建成功
     * @throws SftpException 异常
     */
    private static boolean mkdirs(final String dir, final ChannelSftp sftp) throws SftpException {
        String dirs = dir.substring(1, dir.length() - 1);
        String[] dirArr = dirs.split("/");
        String base = "";
        for (String d : dirArr) {
            base += "/" + d;
            if (dirExist(base + "/", sftp)) {
                continue;
            } else {
                sftp.mkdir(base + "/");
            }
        }
        return true;
    }

    /**
     * 判断文件夹是否存在.
     *
     * @param dir  文件夹路径， /xxx/xxx/
     * @param sftp sftp协议
     * @return 是否存在
     */
    public static boolean dirExist(final String dir, final ChannelSftp sftp) {
        try {
            Vector<?> vector = sftp.ls(dir);
            if (null == vector) {
                return false;
            } else {
                return true;
            }
        } catch (SftpException e) {
            return false;
        }
    }

    /**
     * 格式化路径.
     *
     * @param srcPath 原路径. /xxx/xxx/xxx.yyy 或 X:/xxx/xxx/xxx.yy
     * @return list, 第一个是路径（/xxx/xxx/）,第二个是文件名（xxx.yy）
     */
    public static List<String> formatPath(final String srcPath) {
        List<String> list = new ArrayList<String>(2);
        String repSrc = srcPath.replaceAll("\\\\", "/");
        int firstP = repSrc.indexOf("/");
        int lastP = repSrc.lastIndexOf("/");
        String fileName = lastP + 1 == repSrc.length() ? "" : repSrc.substring(lastP + 1);
        String dir = firstP == -1 ? "" : repSrc.substring(firstP, lastP);
        dir = PRE_FIX + (dir.length() == 1 ? dir : (dir + "/"));
        list.add(dir);
        list.add(fileName);
        return list;
    }

    /**
     * 关闭协议-sftp协议.(关闭会导致连接池异常，因此不建议用户自定义关闭)
     *
     * @param sftp sftp连接
     */
    public static void exit(final ChannelSftp sftp) {
        sftp.exit();
    }

    public static InputStream downByte(String fileabsoluteName, ChannelSftp sftp) {
        InputStream in = null;
        try {
            in = sftp.get(fileabsoluteName);
            return in;
        } catch (SftpException e) {
            logger.error("down ftp file error.", e.toString());
        }
        return in;
    }

    public static List getAllFileName(String fildir, ChannelSftp sftp) throws SftpException {
        List<String> fileNames = new LinkedList<>();
        Vector vector = sftp.ls(fildir);
        if (vector != null && vector.size() > 0) {
            vector.stream().forEach(v -> {
                String fileName = ((ChannelSftp.LsEntry) v).getFilename();
                if (StringUtils.equals(".", fileName) || StringUtils.equals("..", fileName) || StringUtils.equals("", fileName) || StringUtils.isBlank(fileName)) {

                } else {
                    fileNames.add(fileName);
                }
            });
            return fileNames;
        }
        return null;
    }

    /**
     * 删除ftp上，指定文件
     **/
    public static void delete(String deleteFile, ChannelSftp sftp)
            throws SftpException {
        Vector content = sftp.ls(deleteFile);
        if (content != null) {
            sftp.rm(deleteFile);
        }
    }


    /**
     * 通过SFTP下载文件
     *
     * @param sftp
     * @param directory     目录
     * @param downloadFile
     * @param saveDirectory
     * @throws Exception
     */
    public void download(final ChannelSftp sftp, String directory, String downloadFile, String saveDirectory) throws Exception {
        String saveFile = saveDirectory + "//" + downloadFile;
        sftp.cd(directory);
        File file = new File(saveFile);
        sftp.get(downloadFile, new FileOutputStream(file));
    }

    /**
     * 得到远程文件大小
     *
     * @param srcSftpFilePath
     * @return 返回文件大小，如返回-2 文件不存在，-1文件读取异常
     * @throws SftpException
     */
    public static long getFileSize(final ChannelSftp sftp, String srcSftpFilePath) throws SftpException {
        long filesize = 0;//文件大于等于0则存在
        try {
            SftpATTRS sftpATTRS = sftp.lstat(srcSftpFilePath);
            filesize = sftpATTRS.getSize();
        } catch (Exception e) {
            filesize = -1;//获取文件大小异常
            if (e.getMessage().toLowerCase().equals("no such file")) {
                filesize = -2;//文件不存在
            }
        }
        return filesize;
    }

}