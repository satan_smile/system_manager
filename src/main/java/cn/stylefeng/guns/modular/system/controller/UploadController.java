package cn.stylefeng.guns.modular.system.controller;

import cn.stylefeng.guns.config.properties.FtpConfigProperties;
import cn.stylefeng.guns.core.util.SftpUtil;
import com.jcraft.jsch.ChannelSftp;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.time.LocalDate;

/**
 * @Description TODO
 * @Author LiuJiaPeng
 * @Date 2018-12-27 16:42
 */
@Controller
@RequestMapping(value = "upload")
public class UploadController {

    private static final String PREFIX = "/system/upload/";
    @Autowired
    private FtpConfigProperties ftp;

    @GetMapping(value = "miner/package")
    public String minerPackage(){
        return PREFIX + "miner_package.html";
    }

    @PostMapping(value = "miner/package")
    @ResponseBody
    public Object minerPackage(String a){
        return null;
    }

    @GetMapping(value = "okcare")
    public String okcare(){
        return PREFIX + "okcare.html";
    }

    @PostMapping(value = "okcare")
    @ResponseBody
    @ApiOperation(value = "病毒库上传")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "version", value = "病毒库版本", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "query", name = "md5", value = "病毒库md5值", dataType = "String", required = true)
    })
    public String okcare(@ApiParam(name = "file",value = "病毒库文件",required = true) MultipartFile file,
                                 @RequestParam String version){
//        ChannelSftp client = null;
//        try {
//            String realName = file.getOriginalFilename();
//            String path = ftp.getCatalog().getOkcare() + realName;
//            LocalDate localDate = LocalDate.now();
//            localDate.getYear() + localDate.getMonthValue() + localDate.getDayOfMonth() + ;
//            File f2 = new File(path);
//            file.transferTo(f2);
//            String targetPath = okkongProperties.getGoFileOkcareCatalog() + "/";
//            client = SftpUtil.getSftpConnect(ftp.getHost(),ftp.getPort(),ftp.getUserName(),ftp.getPassword());
//            SftpUtil.uploadFile(path,targetPath,realName,client);
//
//            String urlPath = new StringBuilder(okkongProperties.getGoFileHttpUrl())
//                    .append(SystemParams.OKCARE_DIR)
//                    .append("/")
//                    .append(realName)
//                    .toString();
//            OkcareVersion okcareVersion = new OkcareVersion();
//            okcareVersion.setName(realName);
//            okcareVersion.setMd5(md5);
//            okcareVersion.setVersion(version);
//            okcareVersion.setPath(urlPath);
//            Result<Boolean> res = okcareVersionService.add(okcareVersion);
//            if(res != null && res.getData() != null && res.getData()){
//                result.setCode(ResultCode.SUCCESS.getCode());
//                result.setMsg(ResultCode.SUCCESS.getMsg());
//                result.setData(urlPath);
//            }
//
//        }catch (Exception e){
//            log.error(e.getMessage(),e);
//            result.setCode(ResultCode.SYSTEM_ERROR.getCode());
//            result.setMsg(ResultCode.SYSTEM_ERROR.getMsg());
//        }finally {
//            SftpUtil.exit(client);
//        }
//        return result;
        return null;
    }

}
