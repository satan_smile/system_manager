package cn.stylefeng.guns.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author LiuJiaPeng
 * @Date 2018-12-27 17:26
 */
@Component
@ConfigurationProperties(prefix = "ftp")
@Data
public class FtpConfigProperties implements Serializable {
     private String host;
     private Integer port;
     private String userName;
     private String password;

     private Catalog catalog;

     private String downloadUrl;

     @Data
     public static class Catalog {
         private String base;
         private String minerPackage;
         private String okcare;
     }

}
